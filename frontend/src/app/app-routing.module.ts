import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
 
  {
    path:'called-aprendiz',
    loadChildren: () => import('./components/called/called-aprendiz/called-aprendiz.module')
    .then(m => m.CalledAprendizModule)
  },
  {
    path:'called-instructor',
    loadChildren: () => import('./components/called/called-instructor/called-instructor.module')
    .then(m => m.CalledInstructorModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module')
    .then(m => m.DashboardModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
