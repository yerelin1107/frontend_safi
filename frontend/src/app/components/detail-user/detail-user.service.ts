import { Injectable } from '@angular/core';
import { IDetailUser } from './detailUser';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DetailUserService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IDetailUser[]> {
    return this.http.get<IDetailUser[]>(`${environment.END_POINT}/api/detailUser`)
    .pipe(map(res => {
      return res;
    }));
  }
}

