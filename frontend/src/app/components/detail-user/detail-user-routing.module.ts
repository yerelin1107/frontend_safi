import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailuserListComponent } from './detailuser-list/detailuser-list.component';
import { DetailuserCreateComponent } from './detailuser-create/detailuser-create.component';
import { DetailuserUpdateComponent } from './detailuser-update/detailuser-update.component';
import { DetailuserViewComponent } from './detailuser-view/detailuser-view.component';


const routes: Routes = [
  {
    path: 'detailuser-list',
    component: DetailuserListComponent
  },
  {
    path: 'detailuser-create',
    component: DetailuserCreateComponent
  },
  {
    path: 'detailuser-update',
    component: DetailuserUpdateComponent
  },
  {
    path: 'detailuser-view',
    component: DetailuserViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailUserRoutingModule { }
