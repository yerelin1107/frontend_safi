import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailuserViewComponent } from './detailuser-view.component';

describe('DetailuserViewComponent', () => {
  let component: DetailuserViewComponent;
  let fixture: ComponentFixture<DetailuserViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailuserViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailuserViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
