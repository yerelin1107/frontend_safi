import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailuserCreateComponent } from './detailuser-create.component';

describe('DetailuserCreateComponent', () => {
  let component: DetailuserCreateComponent;
  let fixture: ComponentFixture<DetailuserCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailuserCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailuserCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
