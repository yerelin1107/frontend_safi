import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailUserRoutingModule } from './detail-user-routing.module';
import { DetailuserListComponent } from './detailuser-list/detailuser-list.component';
import { DetailuserCreateComponent } from './detailuser-create/detailuser-create.component';
import { DetailuserUpdateComponent } from './detailuser-update/detailuser-update.component';
import { DetailuserViewComponent } from './detailuser-view/detailuser-view.component';


@NgModule({
  declarations: [DetailuserListComponent, DetailuserCreateComponent, DetailuserUpdateComponent, DetailuserViewComponent],
  imports: [
    CommonModule,
    DetailUserRoutingModule
  ]
})
export class DetailUserModule { }
