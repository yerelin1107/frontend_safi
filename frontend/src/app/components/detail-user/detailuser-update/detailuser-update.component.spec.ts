import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailuserUpdateComponent } from './detailuser-update.component';

describe('DetailuserUpdateComponent', () => {
  let component: DetailuserUpdateComponent;
  let fixture: ComponentFixture<DetailuserUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailuserUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailuserUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
