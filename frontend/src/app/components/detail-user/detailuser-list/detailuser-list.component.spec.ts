import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailuserListComponent } from './detailuser-list.component';

describe('DetailuserListComponent', () => {
  let component: DetailuserListComponent;
  let fixture: ComponentFixture<DetailuserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailuserListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailuserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
