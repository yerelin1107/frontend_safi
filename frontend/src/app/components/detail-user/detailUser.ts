import { IUsers } from "../users/users";

export interface IDetailUser{
  id?: number,
  name?: string,
  documentNumber?: string,
  phone?: string,
  emailPersonal?: string,
  address?: string,
  gender?: string,
  documentType?: string,
  emailSena?: string,
  users?: IUsers
}
