import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamfileUpdateComponent } from './teamfile-update.component';

describe('TeamfileUpdateComponent', () => {
  let component: TeamfileUpdateComponent;
  let fixture: ComponentFixture<TeamfileUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamfileUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamfileUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
