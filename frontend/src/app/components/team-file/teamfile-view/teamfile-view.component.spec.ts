import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamfileViewComponent } from './teamfile-view.component';

describe('TeamfileViewComponent', () => {
  let component: TeamfileViewComponent;
  let fixture: ComponentFixture<TeamfileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamfileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
