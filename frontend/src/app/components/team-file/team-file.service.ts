import { Injectable } from '@angular/core';
import { ITeamFile } from './teamFile';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamFileService {

  constructor(private http: HttpClient) { }

  public query(): Observable<ITeamFile[]> {
    return this.http.get<ITeamFile[]>(`${environment.END_POINT}/api/teamFile`)
    .pipe(map(res => {
      return res;
    }));
  }
}

