import { TestBed } from '@angular/core/testing';

import { TeamFileService } from './team-file.service';

describe('TeamFileService', () => {
  let service: TeamFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeamFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
