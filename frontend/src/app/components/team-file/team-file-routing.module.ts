import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamfileListComponent } from './teamfile-list/teamfile-list.component';
import { TeamfileCreateComponent } from './teamfile-create/teamfile-create.component';
import { TeamfileUpdateComponent } from './teamfile-update/teamfile-update.component';
import { TeamfileViewComponent } from './teamfile-view/teamfile-view.component';


const routes: Routes = [
  {
    path: 'teamfile-list',
    component: TeamfileListComponent
  },
  {
    path: 'teamfile-create',
    component: TeamfileCreateComponent
  },
  {
    path: 'teamfile-update',
    component: TeamfileUpdateComponent
  },
  {
    path: 'teamfile-view',
    component: TeamfileViewComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamFileRoutingModule { }
