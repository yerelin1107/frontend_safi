import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamfileListComponent } from './teamfile-list.component';

describe('TeamfileListComponent', () => {
  let component: TeamfileListComponent;
  let fixture: ComponentFixture<TeamfileListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamfileListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamfileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
