import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamFileRoutingModule } from './team-file-routing.module';
import { TeamfileCreateComponent } from './teamfile-create/teamfile-create.component';
import { TeamfileListComponent } from './teamfile-list/teamfile-list.component';
import { TeamfileUpdateComponent } from './teamfile-update/teamfile-update.component';
import { TeamfileViewComponent } from './teamfile-view/teamfile-view.component';


@NgModule({
  declarations: [TeamfileCreateComponent, TeamfileListComponent, TeamfileUpdateComponent, TeamfileViewComponent],
  imports: [
    CommonModule,
    TeamFileRoutingModule
  ]
})
export class TeamFileModule { }
