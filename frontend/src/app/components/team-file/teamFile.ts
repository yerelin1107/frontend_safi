import { ICalled } from "../called/called";
import { IFile } from "../file/file";

export interface ITeamFile{
  id?: number,
  //rolUserFile?:,
  called?: ICalled,
  file?: IFile
}
