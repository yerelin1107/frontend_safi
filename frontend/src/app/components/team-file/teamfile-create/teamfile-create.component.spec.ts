import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamfileCreateComponent } from './teamfile-create.component';

describe('TeamfileCreateComponent', () => {
  let component: TeamfileCreateComponent;
  let fixture: ComponentFixture<TeamfileCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamfileCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamfileCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
