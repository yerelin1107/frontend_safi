import { Component, OnInit } from '@angular/core';
import { IFile } from '../file';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from '../file.service';
@Component({
  selector: 'app-file-create',
  templateUrl: './file-create.component.html',
  styleUrls: ['./file-create.component.styl']
})
export class FileCreateComponent implements OnInit {
  FileFormGroup: FormGroup;
  file: IFile;

  constructor(private formBuilder: FormBuilder, private fileService: FileService) {
    this.FileFormGroup = this.formBuilder.group({
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
   }

  ngOnInit() {
  }

  saveFile() {
    console.log('DATOS', this.FileFormGroup.value);
    this.fileService.saveFile(this.FileFormGroup.value)
    .subscribe(res => {
        console.log('SAVE OK ', res);

    }, error => {

      console.error('Error', error);
    });
  }

}
