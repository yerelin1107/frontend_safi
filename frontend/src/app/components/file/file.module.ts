import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileRoutingModule } from './file-routing.module';
import { FileListComponent } from './file-list/file-list.component';
import { FileCreateComponent } from './file-create/file-create.component';
import { FileViewComponent } from './file-view/file-view.component';
import { FileUpdateComponent } from './file-update/file-update.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [FileListComponent, FileCreateComponent, FileViewComponent, FileUpdateComponent],
  
  imports: [
    CommonModule,
    FileRoutingModule,
    ReactiveFormsModule
  ]
})
export class FileModule { }
