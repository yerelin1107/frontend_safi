import { Component, OnInit } from '@angular/core';
import { IFile } from '../file';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileService } from '../file.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-file-update',
  templateUrl: './file-update.component.html',
  styleUrls: ['./file-update.component.styl']
})
export class FileUpdateComponent implements OnInit {
  FileFormGroup: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    private fileService: FileService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
    this.FileFormGroup = this.formBuilder.group({
      id: [''],
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.fileService.getFileById(id)
    .subscribe(res => {
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }

  saveFile() {
    console.log('DATOS', this.FileFormGroup.value);
    this.fileService.updateFile(this.FileFormGroup.value)
    .subscribe(res => {
        console.log('UPDATE OK ', res);
        this.router.navigate(['/file/file-list']);
    }, error => {
      console.error('Error', error);
    });
  }

  private loadForm(File: IFile){
    this.FileFormGroup.patchValue({
      id: File.id,
      lastName: File.code
    });
  }
}

               