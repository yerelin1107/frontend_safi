import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IFile } from './file';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/request.utils';
@Injectable({
  providedIn: 'root'
})
export class FileService {
  constructor(private http: HttpClient) { }

  /**
   *
   * @param req
   */

  public queryFile(req?: any): Observable<IFile[]> {
    let params = createRequestParams(req);
    return this.http.get<IFile[]>(`${environment.END_POINT}/api/file`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   *
   * @param IFile
   */

  public saveFile(IFile: IFile): Observable<IFile> {
    return this.http.post<IFile>(`${environment.END_POINT}/api/file`, IFile)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IUsers for id
   * @param id
   */ 

  public getFileById(id: string): Observable<IFile> {
    return this.http.get<IFile>(`${environment.END_POINT}/api/file/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }


  /**
   * this... UPDATE
   * @param IFile
   *
   */
  public updateFile(IFile: IFile): Observable<IFile>{
    return this.http.put<IFile>(`${environment.END_POINT}/api/file`, IFile)
      .pipe(map(res => {
        return res;
      }));
  }

    /**
     *
     * @param id
     */

  public deleteItem(id: string): Observable<IFile> {
    return this.http.delete<IFile>(`${environment.END_POINT}/api/file/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

}



