import { Component, OnInit } from '@angular/core';
import { IFile } from '../file';
import { FileService } from '../file.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.styl']
})
export class FileListComponent implements OnInit {

   
  public fileList: IFile[];

  pageSize = 3;
  pageNumber = 2;

  listPages =  [];
  constructor(private fileService: FileService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  initPagination(page: number): void {
    this.fileService.queryFile({
      'pageSize': this.pageSize,
      
    })
    .subscribe((res: any) => {
      console.warn('Get Data ',res);
      this.fileList = res.content;
      
    });
  }


  deleteItem(id: string) {
    console.warn('ID ',id);
    this.fileService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}



