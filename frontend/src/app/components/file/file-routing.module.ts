import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FileListComponent } from './file-list/file-list.component';
import { FileCreateComponent } from './file-create/file-create.component';
import { FileUpdateComponent } from './file-update/file-update.component';
import { FileViewComponent } from './file-view/file-view.component';


const routes: Routes = [
  {
    path: 'file-list',
    component: FileListComponent
  },
  {
    path: 'file-create',
    component: FileCreateComponent
  },
  {
    path: 'file-update',
    component: FileUpdateComponent
  },
  {
    path: 'file-view',
    component: FileViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileRoutingModule { }
