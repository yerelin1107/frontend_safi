import { IProgram } from "../program/program";
import { IProject } from "../project/project";

export interface IFile{
  id?: number,
  code?: string,
  workingDay?: string,
  RolUserFile?: string,
  program?: IProgram,
  project?: IProject
}
