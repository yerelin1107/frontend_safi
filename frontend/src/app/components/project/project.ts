export interface IProject{
  id?: number,
  date?: Date,
  description?: string
}
