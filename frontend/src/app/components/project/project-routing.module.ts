import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectUpdateComponent } from './project-update/project-update.component';
import { ProjectViewComponent } from './project-view/project-view.component';


const routes: Routes = [
  {
    path: 'project-list',
    component: ProjectListComponent
  },
  {
    path: 'project-create',
    component: ProjectCreateComponent
  },
  {
    path: 'project-update',
    component: ProjectUpdateComponent
  },
  {
    path: 'project-view',
    component: ProjectViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
