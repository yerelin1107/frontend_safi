import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProject } from './project';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IProject[]> {
    return this.http.get<IProject[]>(`${environment.END_POINT}/api/project`)
    .pipe(map(res => {
      return res;
    }))
  }
}
