import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramRoutingModule } from './program-routing.module';
import { ProgramCreateComponent } from './program-create/program-create.component';
import { ProgramListComponent } from './program-list/program-list.component';
import { ProgramUpdateComponent } from './program-update/program-update.component';
import { ProgramViewComponent } from './program-view/program-view.component';


@NgModule({
  declarations: [ProgramCreateComponent, ProgramListComponent, ProgramUpdateComponent, ProgramViewComponent],
  imports: [
    CommonModule,
    ProgramRoutingModule
  ]
})
export class ProgramModule { }
