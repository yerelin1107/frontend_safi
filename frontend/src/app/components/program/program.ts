export interface IProgram{
  id?: number,
  code?: string,
  version?: string,
  nameProgram?: string,
  durationTrimesterLective?: number,
  status?: Boolean,
  justification?: string,
  durationTrimesterProductive?: number
}
