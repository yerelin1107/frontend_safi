import { Injectable } from '@angular/core';
import { IProgram } from './program';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IProgram[]> {
    return this.http.get<IProgram[]>(`${environment.END_POINT}/api/program`)
    .pipe(map(res => {
      return res;
    }));
  }
}

