import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramListComponent } from './program-list/program-list.component';
import { ProgramCreateComponent } from './program-create/program-create.component';
import { ProgramUpdateComponent } from './program-update/program-update.component';
import { ProgramViewComponent } from './program-view/program-view.component';


const routes: Routes = [
  {
    path: 'program-list',
    component: ProgramListComponent
  },
  {
    path: 'program-create',
    component: ProgramCreateComponent
  },
  {
    path: 'program-update',
    component: ProgramUpdateComponent
  },
  {
    path: 'program-view',
    component: ProgramViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramRoutingModule { }
