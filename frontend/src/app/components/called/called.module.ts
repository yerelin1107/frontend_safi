import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalledRoutingModule } from './called-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CalledAprendizDiligenciarComponent } from './called-aprendiz/called-aprendiz-diligenciar/called-aprendiz-diligenciar.component';
import { CalledInstructorCreateComponent } from './called-instructor/called-instructor-create/called-instructor-create.component';
import { CalledInstructorUpdateComponent } from './called-instructor/called-instructor-update/called-instructor-update.component';
import { CalledInstructorListComponent } from './called-instructor/called-instructor-list/called-instructor-list.component';
import { CalledInstructorViewComponent } from './called-instructor/called-instructor-view/called-instructor-view.component';
import { CalledaprendizListComponent } from './called-aprendiz/calledaprendiz-list/calledaprendiz-list.component';

@NgModule({
  declarations: [CalledAprendizDiligenciarComponent,
      CalledaprendizListComponent,
      CalledInstructorCreateComponent,
      CalledInstructorUpdateComponent,
      CalledInstructorListComponent,
      CalledInstructorViewComponent],
  imports: [
    CommonModule,
    CalledRoutingModule,
    ReactiveFormsModule,
  ]
})
export class CalledModule { }
