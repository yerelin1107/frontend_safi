import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { CalledAprendizService} from '../called-aprendiz.service';
import { ICalled } from '../../called';
import { IDetailUser } from 'src/app/components/detail-user/detailUser';

@Component({
  selector: 'app-called-aprendiz-diligenciar',
  templateUrl: './called-aprendiz-diligenciar.component.html',
  styleUrls: ['./called-aprendiz-diligenciar.component.styl']
})
export class CalledAprendizDiligenciarComponent implements OnInit {
  CalledFormGroup: FormGroup;
  instructor: IDetailUser;
  detailUser: IDetailUser;
  called: ICalled;


  constructor(private formBuilder: FormBuilder, private  calledaprendizService :CalledAprendizService) {
    this.CalledFormGroup = this.formBuilder.group({
      name:['',[Validators.required]],
      documentNumber:['',[Validators.required]],
      emailSena:['',[Validators.required]],
      emailPersonal:['',[Validators.required]],
      File:['',[Validators.required]],
      project:['',[Validators.required]],
      situation:[''],
      impact:[''],
      effect:[''],
      calledType:['',[Validators.required]]
    });
  }

  ngOnInit(){
  }

  sendCalled(){
    this.calledaprendizService.queryCalled(this.CalledFormGroup.value)
    .subscribe(res => {
      console.log('SAVE OK ', res);
    },
    error => {
    console.error('Error', error);
    });

  }

  saveCalled() {
    console.log('DATOS', this.CalledFormGroup.value);
    this.calledaprendizService.saveCalled(this.CalledFormGroup.value)
    .subscribe(res => {
      console.log('SAVE OK ', res);

    }, error => {
      console.error('Error', error);
    });
  }

}
