import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledAprendizDiligenciarComponent } from './called-aprendiz-diligenciar.component';

describe('CalledAprendizDiligenciarComponent', () => {
  let component: CalledAprendizDiligenciarComponent;
  let fixture: ComponentFixture<CalledAprendizDiligenciarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledAprendizDiligenciarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledAprendizDiligenciarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
