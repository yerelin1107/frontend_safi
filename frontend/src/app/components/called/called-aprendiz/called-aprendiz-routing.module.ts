import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainAprendizComponent } from './main-aprendiz/main-aprendiz.component';
import { CalledaprendizListComponent } from './calledaprendiz-list/calledaprendiz-list.component';
import { CalledAprendizDiligenciarComponent } from './called-aprendiz-diligenciar/called-aprendiz-diligenciar.component';
const routes: Routes = [
  {
    path:'main-aprendiz',
    component: MainAprendizComponent,
    children:[
      {
        path: 'calledaprendiz-list',
        component: CalledaprendizListComponent

      },
      {
        path: 'called-aprendiz-diligenciar',
        component: CalledAprendizDiligenciarComponent

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalledAprendizRoutingModule { }
