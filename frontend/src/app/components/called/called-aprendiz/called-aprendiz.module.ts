import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalledAprendizRoutingModule } from './called-aprendiz-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CalledAprendizDiligenciarComponent } from './called-aprendiz-diligenciar/called-aprendiz-diligenciar.component';
import { CalledaprendizListComponent } from './calledaprendiz-list/calledaprendiz-list.component';
import { MainAprendizComponent } from './main-aprendiz/main-aprendiz.component';
import {PaginatorModule} from 'primeng/paginator';
import {AutoCompleteModule} from 'primeng/autocomplete';


@NgModule({
  declarations: [ CalledAprendizDiligenciarComponent, CalledaprendizListComponent, MainAprendizComponent],
  imports: [
    CommonModule,
    CalledAprendizRoutingModule,
    ReactiveFormsModule,
    PaginatorModule,
    AutoCompleteModule
  ]
})
export class CalledAprendizModule { }
