import { Component, OnInit } from '@angular/core';
import { ICalled } from '../../called';
import { CalledAprendizService } from '../called-aprendiz.service';

@Component({
  selector: 'app-calledaprendiz-list',
  templateUrl: './calledaprendiz-list.component.html',
  styleUrls: ['./calledaprendiz-list.component.styl']
})
export class CalledaprendizListComponent implements OnInit {


  public calledaprendizList: ICalled[];

  pageSize = 8;
  pageNumber = 0;

  listPages = []; // list pages for buttons
  constructor(private calledaprendizService: CalledAprendizService) {
   }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  /**
   *
   * @param page
   */

  initPagination(page: number): void {
    this.calledaprendizService.queryCalled({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      console.warn('DATOS ',res);
      this.calledaprendizList = res.content;
      this.formatPage(res.totalPages);
    });
  }

  /**
   *
   * @param delete Item by id
   */

  deleteItem(id: string) {
    console.warn('ID ',id);
    this.calledaprendizService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  /**
   *
   * @param countPages
   */

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}

