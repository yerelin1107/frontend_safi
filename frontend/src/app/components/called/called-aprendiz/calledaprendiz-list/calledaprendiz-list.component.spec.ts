import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledaprendizListComponent } from './calledaprendiz-list.component';

describe('CalledaprendizListComponent', () => {
  let component: CalledaprendizListComponent;
  let fixture: ComponentFixture<CalledaprendizListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledaprendizListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledaprendizListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
