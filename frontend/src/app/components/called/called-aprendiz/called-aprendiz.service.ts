import { Injectable } from '@angular/core';
import { ICalled } from '../called';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { createRequestParams } from 'src/app/utils/request.utils';
@Injectable({
  providedIn: 'root'
})
export class CalledAprendizService {
  constructor(private http: HttpClient) { }


  /**
   *
   * @param req
   */

  public queryCalled(req?: any): Observable<ICalled[]> {
    let params = createRequestParams(req);
    return this.http.get<ICalled[]>(`${environment.END_POINT}/api/called`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   *
   * @param ICalled
   */

  public saveCalled(ICalled: ICalled): Observable<ICalled> {
    return this.http.post<ICalled>(`${environment.END_POINT}/api/called`, ICalled)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IUsers for id
   * @param id
   */

  public getCalledById(id: string): Observable<ICalled> {
    return this.http.get<ICalled>(`${environment.END_POINT}/api/called/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }

  /**
   *
   * @param updateCalled
   */

  public updateCalled(ICalled: ICalled): Observable<ICalled>{
    return this.http.put<ICalled>(`${ environment.END_POINT }/api/called`, ICalled)
    .pipe(map(res => {
      return res;
    }));
  }

  /**
   *
   * @param delete
   */

  public deleteItem(id: string): Observable<ICalled> {
    return this.http.delete<ICalled>(`${environment.END_POINT}/api/called/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
}


