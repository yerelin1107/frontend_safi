import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAprendizComponent } from './main-aprendiz.component';

describe('MainAprendizComponent', () => {
  let component: MainAprendizComponent;
  let fixture: ComponentFixture<MainAprendizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainAprendizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAprendizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
