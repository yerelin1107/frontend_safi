import { TestBed } from '@angular/core/testing';

import { CalledAprendizService } from './called-aprendiz.service';

describe('CalledAprendizService', () => {
  let service: CalledAprendizService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalledAprendizService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
