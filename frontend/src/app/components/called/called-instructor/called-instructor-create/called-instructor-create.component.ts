import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { CalledInstructorService } from '../called-instructor.service';
import { ICalled } from '../../called';
import { IFile } from 'src/app/components/file/file';
import { CalledService } from '../../called.service';
import { IDetailUser } from 'src/app/components/detail-user/detailUser';


@Component({
  selector: 'app-called-instructor-create',
  templateUrl: './called-instructor-create.component.html',
  styleUrls: ['./called-instructor-create.component.styl']
})
export class CalledInstructorCreateComponent implements OnInit {
  CalledFormGroup: FormGroup;
  instructor: IDetailUser;
  code: IFile;


  constructor(private formBuilder: FormBuilder, private  calledinstructorService :CalledService) {
    this.CalledFormGroup = this.formBuilder.group({
      situation:[''],
      cause:[''],
      impact:[''],
      effect:[''],
      compromise: [''],
      detailUser: [''],
      instructor: [''],
      file: [''],
      calledType:['']
    });
  }

  ngOnInit(){
  }

  saveCalled() {
    console.log('DATOS', this.CalledFormGroup.value);
    this.calledinstructorService.saveCalled(this.CalledFormGroup.value)
    .subscribe(res => {
      console.log('SAVE OK ', res);

    }, error => {
      console.error('Error', error);
    });
  }

}
