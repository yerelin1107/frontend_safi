import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledInstructorCreateComponent } from './called-instructor-create.component';

describe('CalledInstructorCreateComponent', () => {
  let component: CalledInstructorCreateComponent;
  let fixture: ComponentFixture<CalledInstructorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledInstructorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledInstructorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
