import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CalledInstructorRoutingModule } from './called-instructor-routing.module';
import { CalledInstructorListComponent } from './called-instructor-list/called-instructor-list.component';
import { CalledInstructorCreateComponent } from './called-instructor-create/called-instructor-create.component';
import { CalledInstructorUpdateComponent } from './called-instructor-update/called-instructor-update.component';
import { MainInstructorComponent } from './main-instructor/main-instructor.component';
import { CalledInstructorViewComponent } from './called-instructor-view/called-instructor-view.component';
import {PaginatorModule} from 'primeng/paginator';
import {AutoCompleteModule} from 'primeng/autocomplete';



@NgModule({
  declarations: [CalledInstructorListComponent, CalledInstructorCreateComponent, CalledInstructorUpdateComponent, MainInstructorComponent, CalledInstructorViewComponent],
  imports: [
    CommonModule,
    CalledInstructorRoutingModule,
    ReactiveFormsModule,
    PaginatorModule,
    AutoCompleteModule
  ]
})
export class CalledInstructorModule { }
