import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledInstructorListComponent } from './called-instructor-list.component';

describe('CalledInstructorListComponent', () => {
  let component: CalledInstructorListComponent;
  let fixture: ComponentFixture<CalledInstructorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledInstructorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledInstructorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
