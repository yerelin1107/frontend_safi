import { Component, OnInit } from '@angular/core';
import { CalledInstructorService } from '../called-instructor.service';
import {ICalled} from 'src/app/components/called/called';
import { FormBuilder } from '@angular/forms';
import { IDetailUser } from 'src/app/components/detail-user/detailUser';
import { IFile } from 'src/app/components/file/file';
import { IProgram } from 'src/app/components/program/program';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-called-instructor-list',
  templateUrl: './called-instructor-list.component.html',
  styleUrls: ['./called-instructor-list.component.styl']
})
export class CalledInstructorListComponent implements OnInit {
  calledinstructorList: ICalled[];
  detailUserList: IDetailUser[];
  fileList: IProgram[];
  aprendizSelectedTemp: any;
  fichaSelectedTemp: any;

  pageSize = 2;
  pageNumber = 0;
  totalRecords: any;

  searchForm = this.fb.group({
    document: [''],
    code: ['']
  });


  constructor(
  private calledinstructorService: CalledInstructorService,
  private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.loadingPagenation({page: this.pageNumber});
  } // end onInit

  loadingPagenation(event: any): void {
    console.log("Event ", event);
    this.calledinstructorService.queryCalled({
      pageSize: this.pageSize,
      pageNumber: event.page
    })
    .subscribe((res: any) => {

      console.log('GET DATA ', res);
      this.calledinstructorList = res.content;
      this.totalRecords = res.totalElements;

    }, error => {
      console.error("Error ", error);
    });
  }


  searchAprendiz(event : any): void{
    console.log('Event', event);
    let documentNumber =event.query;
    console.info(documentNumber);
    this.calledinstructorService.search({
    documentNumber
    })
    .subscribe(res => {
      this.detailUserList = res;
      console.warn('RES', this.detailUserList);
    });
  }

  searchFicha(event : any): void{
    console.log('Event', event);
    let code =event.query;
    console.info(code);
    this.calledinstructorService.searchCode({
    code
    })
    .subscribe(res => {
      this.fileList = res;
      console.warn('RES', this.fileList);
    });
  }

  selectedAprendiz(aprendiz: any): void {
    console.warn('Selected aprendiz',aprendiz);
    this.aprendizSelectedTemp = aprendiz;
  }

  selectedFicha(ficha: any): void {
    console.warn('Selected ficha',ficha);
    this.fichaSelectedTemp = ficha;
  }

  /**
   *
   * @param id
   */

  deleteItem(id: string) {
    console.warn('ID ',id);
    this.calledinstructorService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }
}
