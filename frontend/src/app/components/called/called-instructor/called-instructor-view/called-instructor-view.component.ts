import { Component, OnInit } from '@angular/core';
import { ICalled } from '../../called';
import { CalledInstructorService } from '../called-instructor.service';
@Component({
  selector: 'app-called-instructor-view',
  templateUrl: './called-instructor-view.component.html',
  styleUrls: ['./called-instructor-view.component.styl']
})
export class CalledInstructorViewComponent implements OnInit {

  
  public calledinstructorView: ICalled[];

  pageSize = 3;
  pageNumber = 0;

  listPages = []; // list pages for buttons
  constructor(private calledinstructorService: CalledInstructorService) {
   }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  /**
   *
   * @param page
   */

  initPagination(page: number): void {
    this.calledinstructorService.queryCalled({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      console.warn('Called ',res);
      this.calledinstructorView = res.content;
      this.formatPage(res.totalPages);
    });
  }

  /**
   *
   * @param delete Item by id
   */

  deleteItem(id: string) {
    console.warn('ID ',id);
    this.calledinstructorService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  /**
   *
   * @param countPages
   */

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}

