import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledInstructorViewComponent } from './called-instructor-view.component';

describe('CalledInstructorViewComponent', () => {
  let component: CalledInstructorViewComponent;
  let fixture: ComponentFixture<CalledInstructorViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledInstructorViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledInstructorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
