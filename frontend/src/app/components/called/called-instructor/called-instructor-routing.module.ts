import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalledInstructorListComponent } from './called-instructor-list/called-instructor-list.component';
import { CalledInstructorCreateComponent } from './called-instructor-create/called-instructor-create.component';
import { CalledInstructorUpdateComponent } from './called-instructor-update/called-instructor-update.component';
import { MainInstructorComponent } from './main-instructor/main-instructor.component';
import { CalledInstructorViewComponent } from './called-instructor-view/called-instructor-view.component';


const routes: Routes = [
  {
path:'main-instructor',
component: MainInstructorComponent,
children:[
  {
    path: 'called-instructor-list',
    component: CalledInstructorListComponent

  },
  {
    path: 'called-instructor-create',
    component: CalledInstructorCreateComponent
  },
  {
    path: 'called-instructor-update',
    component: CalledInstructorUpdateComponent
  },
  {
    path: 'called-instructor-view',
    component: CalledInstructorViewComponent
  }
]
} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalledInstructorRoutingModule { }
