import { Injectable } from '@angular/core';
import { ICalled } from '../called';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { createRequestParams } from 'src/app/utils/request.utils';
import { IDetailUser } from '../../detail-user/detailUser';
import { IProgram } from '../../program/program';
@Injectable({
  providedIn: 'root'
})
export class CalledInstructorService {


  constructor(private http: HttpClient) { }


  /**
   *
   * @param req
   */

  public queryCalled(req?: any): Observable<ICalled[]> {
    let params = createRequestParams(req);
    return this.http.get<ICalled[]>(`${environment.END_POINT}/api/called`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }
  public search (req?:any): Observable<IDetailUser[]>{
    let params = createRequestParams(req);
    return this.http.get<IDetailUser[]>(`${environment.END_POINT}/api/detailUser/search`,{params: params})
    .pipe(map(res =>{
      return res;

    }))
  }

  public searchCode (req?:any): Observable<IProgram[]>{
    let params = createRequestParams(req);
    return this.http.get<IProgram[]>(`${environment.END_POINT}/api/program/search`,{params: params})
    .pipe(map(res =>{
      return res;

    }))
  }

  public findAllByDocumentNumberContains(req?: any): Observable<ICalled[]> {
    let params = createRequestParams(req);
    return this.http.get<ICalled[]>(`${environment.END_POINT}/api/called/find/called-by-document`,{params: params})
    .pipe(map(res => {
      return res;
  }));
}

  /**
   *
   * @param ICalled
   */

  public sendCalled(ICalled: ICalled): Observable<ICalled> {
    return this.http.post<ICalled>(`${environment.END_POINT}/api/called`, ICalled)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IUsers for id
   * @param id
   */

  public getCalledById(id: string): Observable<ICalled> {
    return this.http.get<ICalled>(`${environment.END_POINT}/api/called/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }

  /**
   * this... UPDATE
   * @param ICalled
   *
   */


  public updateCalled(ICalled: ICalled): Observable<ICalled>{
    return this.http.put<ICalled>(`${ environment.END_POINT }/api/called`, ICalled)
    .pipe(map(res => {
      return res;
    }));
  }

  public saveCalled(ICalled: ICalled): Observable<ICalled> {
    return this.http.post<ICalled>(`${environment.END_POINT}/api/called`, ICalled)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   *
   * @param id
   */

  public deleteItem(id: string): Observable<ICalled> {
    return this.http.delete<ICalled>(`${environment.END_POINT}/api/called/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

  }



