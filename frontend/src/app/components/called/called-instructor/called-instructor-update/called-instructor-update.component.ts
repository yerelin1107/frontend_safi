import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { CalledInstructorService } from '../called-instructor.service';

@Component({
  selector: 'app-called-instructor-update',
  templateUrl: './called-instructor-update.component.html',
  styleUrls: ['./called-instructor-update.component.styl']
})
export class CalledInstructorUpdateComponent implements OnInit {
  calledFormGroup = this.formBuilder.group({
    name:['',[Validators.required]],
    documentNumber:['',[Validators.required]],
    emailSena:['',[Validators.required]],
    emailPersonal:['',[Validators.required]],
    File:['',[Validators.required]],
    project:['',[Validators.required]],
    situation:[''],
    impact:[''],
    effect:[''],
    calledType:['',[Validators.required]]
  });

  constructor(private formBuilder: FormBuilder, private  calledinstructorService :CalledInstructorService) {
   }

  ngOnInit(): void { }

  updateCalled(){
    console.warn('CALLED',this.calledFormGroup.value);
    this.calledinstructorService.updateCalled(this.calledFormGroup.value)
    .subscribe(res => {
     console.warn ('SEND OK', res)
    },
    error => {
    console.warn('ERROR' ,error)
    })
  }
  
}
