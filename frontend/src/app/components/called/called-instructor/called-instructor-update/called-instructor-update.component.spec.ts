import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledInstructorUpdateComponent } from './called-instructor-update.component';

describe('CalledInstructorUpdateComponent', () => {
  let component: CalledInstructorUpdateComponent;
  let fixture: ComponentFixture<CalledInstructorUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalledInstructorUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledInstructorUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
