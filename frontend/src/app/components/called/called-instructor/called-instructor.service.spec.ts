import { TestBed } from '@angular/core/testing';

import { CalledInstructorService } from './called-instructor.service';

describe('CalledInstructorService', () => {
  let service: CalledInstructorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalledInstructorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
