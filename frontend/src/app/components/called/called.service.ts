import { Injectable } from '@angular/core';
import { ICalled } from './called';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalledService {

  constructor(private http: HttpClient) { }

  public query(): Observable<ICalled[]> {
    return this.http.get<ICalled[]>(`${environment.END_POINT}/api/called`)
    .pipe(map(res => {
      return res;
    }));
  }
  public sendCalled(called: ICalled):Observable<ICalled[]>{
    return this.http.post<ICalled[]>(`${environment.END_POINT}/api/called`, called)
    .pipe(map(res => {
      return res;
  }));
  }
  public updateCalled(called: ICalled):Observable<ICalled[]>{
    return this.http.post<ICalled[]>(`${environment.END_POINT}/api/called`, called)
    .pipe(map(res => {
      return res;
  }));
}
public send(called: ICalled):Observable<ICalled[]>{
  return this.http.post<ICalled[]>(`${environment.END_POINT}/api/called`, called)
  .pipe(map(res => {
    return res;
}));
}

public saveCalled(ICalled: ICalled): Observable<ICalled> {
  return this.http.post<ICalled>(`${environment.END_POINT}/api/called`, ICalled)
    .pipe(map(res => {
      return res;
    }));
}

}
