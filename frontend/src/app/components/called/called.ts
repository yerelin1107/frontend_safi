import { IDetailUser } from "../detail-user/detailUser";
import { IFile } from '../file/file';

export interface ICalled{
  id?: number,
  situation?: string,
  cause?: string,
  impact?: string,
  effect?: string,
  status?: Boolean,
  date?: Date,
  compromise?: string,
  calledType?: string,
  detailUser?: IDetailUser,
  instructor?:IDetailUser,
  file?:IFile
}
