import { Component, OnInit } from '@angular/core';
import { IUsers } from '../users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.styl']
})
export class UsersListComponent implements OnInit {
  
  public usersList: IUsers[];

  pageSize = 2;
  pageNumber = 2;

  listPages =  [];
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  initPagination(page: number): void {
    this.usersService.queryUsers({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      console.warn('Get Data ',res);
      this.usersList = res.content;
      this.formatPage(res.totalPages);
    });
  }


  deleteItem(id: string) {
    console.warn('ID ',id);
    this.usersService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}



