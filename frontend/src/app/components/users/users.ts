import { IRoles } from "../roles/roles";

export interface IUsers{
  id?: number,
  login?: string,
  password?: string,
  email?: string,
  firstName?: string,
  lastName?: string,
  imageURL?: string,
  rol?: IRoles
}
