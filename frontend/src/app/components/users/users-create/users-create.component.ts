import { Component, OnInit } from '@angular/core';
import { IUsers } from '../users';
import { UsersService } from '../users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.styl']
})
export class UsersCreateComponent implements OnInit {
  UsersFormGroup: FormGroup;
  users: IUsers;

  constructor(private formBuilder: FormBuilder, private usersService: UsersService) {
    this.UsersFormGroup = this.formBuilder.group({
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
   }

  ngOnInit() {
  }

  saveUsers() {
    console.log('DATOS', this.UsersFormGroup.value);
    this.usersService.saveUsers(this.UsersFormGroup.value)
    .subscribe(res => {
        console.log('SAVE OK ', res);

    }, error => {

      console.error('Error', error);
    });
  }

}

  