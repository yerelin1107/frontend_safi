import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUsers } from '../users';
import { UsersService } from '../users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users-update',
  templateUrl: './users-update.component.html',
  styleUrls: ['./users-update.component.styl']
})
export class UsersUpdateComponent implements OnInit {
  UsersFormGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
    this.UsersFormGroup = this.formBuilder.group({
      id: [''],
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.usersService.getUsersById(id)
    .subscribe(res => {
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }

  saveUsers() {
    console.log('DATOS', this.UsersFormGroup.value);
    this.usersService.updateUsers(this.UsersFormGroup.value)
    .subscribe(res => {
        console.log('UPDATE OK ', res);
        this.router.navigate(['/users/users-list']);
    }, error => {
      console.error('Error', error);
    });
  }

  private loadForm(Users: IUsers){
    this.UsersFormGroup.patchValue({
      id: Users.id,
      lastName: Users.lastName
    });
  }
}

  