import { Injectable } from '@angular/core';
import { IUsers } from './users';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { createRequestParams } from 'src/app/utils/request.utils';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) { }

  /**
   *
   * @param req
   */

  public queryUsers(req?: any): Observable<IUsers[]> {
    let params = createRequestParams(req);
    return this.http.get<IUsers[]>(`${environment.END_POINT}/api/user`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   *
   * @param IUsers
   */

  public saveUsers(IUsers: IUsers): Observable<IUsers> {
    return this.http.post<IUsers>(`${environment.END_POINT}/api/user`, IUsers)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IUsers for id
   * @param id
   */

  public getUsersById(id: string): Observable<IUsers> {
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }


  /**
   * this... UPDATE
   * @param IUsers
   *
   */
  public updateUsers(IUsers: IUsers): Observable<IUsers> {
    return this.http.put<IUsers>(`${environment.END_POINT}/api/users`, IUsers)
      .pipe(map(res => {
        return res;
      }));
  }

    /**
     *
     * @param id
     */

  public deleteItem(id: string): Observable<IUsers> {
    return this.http.delete<IUsers>(`${environment.END_POINT}/api/users/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

}



