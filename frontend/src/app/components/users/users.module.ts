import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersCreateComponent } from './users-create/users-create.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersUpdateComponent } from './users-update/users-update.component';
import { UsersViewComponent } from './users-view/users-view.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UsersCreateComponent, UsersListComponent, UsersUpdateComponent, UsersViewComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
