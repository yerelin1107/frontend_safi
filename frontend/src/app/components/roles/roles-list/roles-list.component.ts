import { Component, OnInit } from '@angular/core';
import { IRoles } from '../roles';
import { RolesService } from '../roles.service';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.styl']
})
export class RolesListComponent implements OnInit {

  public rolesList: IRoles[];

  pageSize = 5;
  pageNumber = 0;

  listPages =  [];
  constructor(private rolesService: RolesService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  } // end onInit

  initPagination(page: number): void {
    this.rolesService.queryRoles({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      console.warn('DATOS ',res);
      this.rolesList = res.content;
      this.formatPage(res.totalPages);
    });
  }


  deleteItem(id: string) {
    console.warn('ID ',id);
    this.rolesService.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deleted ok...', res);
      this.ngOnInit();
    }, error => console.warn('Error ',error));
  }

  private formatPage(countPages: number): void {
    this.listPages = [];
    for(let i = 0; i < countPages; i++) {
      this.listPages.push(i);
    }
  }

}
