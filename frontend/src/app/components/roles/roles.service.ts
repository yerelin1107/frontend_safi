import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IRoles } from './roles';
import { environment } from 'src/environments/environment';
import { createRequestParams } from 'src/app/utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient) { }

  /**
   *
   * @param req
   */

  public queryRoles(req?: any): Observable<IRoles[]> {
    let params = createRequestParams(req);
    return this.http.get<IRoles[]>(`${environment.END_POINT}/api/roles`,{params: params})
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   *
   * @param IRoles
   */

  public saveRol(IRoles: IRoles): Observable<IRoles> {
    return this.http.post<IRoles>(`${environment.END_POINT}/api/roles`, IRoles)
      .pipe(map(res => {
        return res;
      }));
  }

  /**
   * This method is for getting one IRoles for id
   * @param id
   */

  public getRolById(id: string): Observable<IRoles> {
    return this.http.get<IRoles>(`${environment.END_POINT}/api/roles/${id}`)
      .pipe(map(res => {
        return res;
      }));

  }


  /**
   * this... UPDATE
   * @param IRoles
   *
   */
  public updateRol(IRoles: IRoles): Observable<IRoles> {
    return this.http.put<IRoles>(`${environment.END_POINT}/api/roles`, IRoles)
      .pipe(map(res => {
        return res;
      }));
  }

    /**
     *
     * @param id
     */

  public deleteItem(id: string): Observable<IRoles> {
    return this.http.delete<IRoles>(`${environment.END_POINT}/api/roles/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

}
