import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IRoles } from '../roles';
import { RolesService } from '../roles.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-roles-update',
  templateUrl: './roles-update.component.html',
  styleUrls: ['./roles-update.component.styl']
})
export class RolesUpdateComponent implements OnInit {
  RolFormGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private rolesService: RolesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
    this.RolFormGroup = this.formBuilder.group({
      id: [''],
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.rolesService.getRolById(id)
    .subscribe(res => {
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }

  saveRol() {
    console.log('DATOS', this.RolFormGroup.value);
    this.rolesService.updateRol(this.RolFormGroup.value)
    .subscribe(res => {
        console.log('UPDATE OK ', res);
        this.router.navigate(['/roles/roles-list']);
    }, error => {
      console.error('Error', error);
    });
  }

  private loadForm(Rol: IRoles){
    this.RolFormGroup.patchValue({
      id: Rol.id,
      description: Rol.description
    });
  }
}
