import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesListComponent } from './roles-list/roles-list.component';
import { RolesCreateComponent } from './roles-create/roles-create.component';
import { RolesUpdateComponent } from './roles-update/roles-update.component';
import { RolesViewComponent } from './roles-view/roles-view.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RolesListComponent,
    RolesCreateComponent,
    RolesUpdateComponent,
    RolesViewComponent
  ],
  imports: [
    CommonModule,
    RolesRoutingModule,
    ReactiveFormsModule
  ]
})
export class RolesModule { }
