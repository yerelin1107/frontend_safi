import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesListComponent } from './roles-list/roles-list.component';
import { RolesCreateComponent } from './roles-create/roles-create.component';
import { RolesUpdateComponent } from './roles-update/roles-update.component';
import { RolesViewComponent } from './roles-view/roles-view.component';


const routes: Routes = [
  {
    path: 'roles-list',
    component: RolesListComponent
  },
  {
    path: 'roles-create',
    component: RolesCreateComponent
  },
  {
    path: 'roles-update',
    component: RolesUpdateComponent
  },
  {
    path: 'roles-view',
    component: RolesViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
