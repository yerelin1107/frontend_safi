import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IRoles } from '../roles';
import { RolesService } from '../roles.service';

@Component({
  selector: 'app-roles-create',
  templateUrl: './roles-create.component.html',
  styleUrls: ['./roles-create.component.styl']
})
export class RolesCreateComponent implements OnInit {
  RolFormGroup: FormGroup;
  rol: IRoles;

  constructor(private formBuilder: FormBuilder, private rolesService: RolesService) {
    this.RolFormGroup = this.formBuilder.group({
      description: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])]
    });
   }

  ngOnInit() {
  }

  saveRol() {
    console.log('DATOS', this.RolFormGroup.value);
    this.rolesService.saveRol(this.RolFormGroup.value)
    .subscribe(res => {
        console.log('SAVE OK ', res);

    }, error => {

      console.error('Error', error);
    });
  }

}
