import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { CalledInstructorListComponent } from '../components/called/called-instructor/called-instructor-list/called-instructor-list.component';
import { CalledInstructorCreateComponent } from '../components/called/called-instructor/called-instructor-create/called-instructor-create.component';
import { CalledInstructorViewComponent } from '../components/called/called-instructor/called-instructor-view/called-instructor-view.component';




const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    children: [
      {
        path: 'called-instructor-list',
        component: CalledInstructorListComponent
      },
      {
        path: 'called-instructor-create',
        component: CalledInstructorCreateComponent
      },
      {
        path: 'called-instructor-view',
        component: CalledInstructorViewComponent
      }
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
