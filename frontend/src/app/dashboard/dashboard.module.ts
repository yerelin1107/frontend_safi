import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { CalledAprendizModule } from '../components/called/called-aprendiz/called-aprendiz.module';
import { CalledInstructorModule } from '../components/called/called-instructor/called-instructor.module';


@NgModule({
  declarations: [SidebarComponent, NavbarComponent, MainDashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CalledInstructorModule,
    CalledAprendizModule
  ]
})
export class DashboardModule { }
